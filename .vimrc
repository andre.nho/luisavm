" files format
set fileformat=unix

set ts=4
set sts=4
set sw=4
set expandtab
set foldmethod=marker

" make program
set makeprg=clear\ \&\&\ make
"nnoremap <Leader>T :!clear && make test<cr>
"nnoremap <Leader>y :!clear && ./las -t<cr>
"nnoremap <Leader>C :make! clean<cr>
nnoremap <Leader>r :!clear && make clean debug && ./luisavm<cr>
nnoremap <Leader>T :!clear && make clean test<cr>
nnoremap <Leader>G :!git commit -a && git push<CR>

" open all files
e src/main.c
args Makefile
args src/*.c
args lib/*.h
args lib/*.c
args .vimrc
b 1
map <Leader>x   :b src/main.c<CR>
map <Leader>t   :b src/tests.c<CR>
map <Leader>c   :b lib/computer.c<CR>

map <Leader>M   :b Makefile<CR>
map <Leader>V   :b .vimrc<CR>

" swap between cc and hh
function! SwapBuffers()
  if expand("%:e") == "c"
    exe "b" fnameescape(expand("%:r").".h")
  elseif expand("%:e") == "h"
    exe "b" fnameescape(expand("%:r").".c")
  endif
endfunction
map <Leader>.  :call SwapBuffers()<CR>
