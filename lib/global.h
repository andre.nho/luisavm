#ifndef LUISAVM_GLOBAL_H_
#define LUISAVM_GLOBAL_H_

#include <stdlib.h>

void* MALLOC(size_t sz);
void* CALLOC(size_t nm, size_t sz);

#endif
