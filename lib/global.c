#include "global.h"

#include <syslog.h>

void* MALLOC(size_t sz)
{
    void* data = malloc(sz);
    if(!data) {
        syslog(LOG_ERR, "no memory left: impossible to allocate %lu bytes", sz);
        abort();
    }
    return data;
}


void* CALLOC(size_t nm, size_t sz)
{
    void* data = calloc(nm, sz);
    if(!data) {
        syslog(LOG_ERR, "no memory left: impossible to allocate %lu bytes", sz);
        abort();
    }
    return data;
}
