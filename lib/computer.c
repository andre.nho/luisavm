#include "computer.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include "global.h"

struct LVM_Computer {
    uint8_t* ram;
    size_t   memory_kb;
};


struct LVM_Computer* 
lvm_computer_create(size_t memory_kb)
{
    struct LVM_Computer* c = (struct LVM_Computer*)CALLOC(sizeof(struct LVM_Computer), 1);
    c->ram = CALLOC(memory_kb * 1024, 1);
    c->memory_kb = memory_kb;
    return c;
}


void
lvm_computer_reset(struct LVM_Computer* c)
{
    memset(c->ram, 0, c->memory_kb * 1024);
}

/* {{{ memory access */

uint8_t 
lvm_computer_get(struct LVM_Computer* c, uint32_t pos)
{
    if(pos < c->memory_kb * 1024) {
        return c->ram[pos];
    }
        
    syslog(LOG_WARNING, "Trying to get memory position above memory size: 0x%08X", pos);
    return 0;
}


void
lvm_computer_set(struct LVM_Computer* c, uint32_t pos, uint8_t data)
{
    if(pos < c->memory_kb * 1024) {
        c->ram[pos] = data;
    } else {
        syslog(LOG_WARNING, "Trying to set memory position above memory size: 0x%08X", pos);
    }
}


uint16_t 
lvm_computer_get16(struct LVM_Computer* c, uint32_t pos)
{
    if(pos+1 < c->memory_kb * 1024) {
        return (c->ram[pos+1] << 8) | c->ram[pos];
    }
        
    syslog(LOG_WARNING, "Trying to get 16-bit memory position above memory size: 0x%08X", pos);
    return 0;
}


void
lvm_computer_set16(struct LVM_Computer* c, uint32_t pos, uint16_t data)
{
    if(pos+1 < c->memory_kb * 1024) {
        c->ram[pos] = (uint8_t)data;
        c->ram[pos+1] = (uint8_t)(data >> 8);
    } else {
        syslog(LOG_WARNING, "Trying to set 16-bit memory position above memory size: 0x%08X", pos);
    }
}


uint32_t 
lvm_computer_get32(struct LVM_Computer* c, uint32_t pos)
{
    if(pos+3 < c->memory_kb * 1024) {
        return ((c->ram[pos+3] << 24))   | 
               ((c->ram[pos+2] << 16)) | 
               ((c->ram[pos+1] << 8))  | 
               c->ram[pos];
    }
        
    syslog(LOG_WARNING, "Trying to get 32-bit memory position above memory size: 0x%08X", pos);
    return 0;
}


void
lvm_computer_set32(struct LVM_Computer* c, uint32_t pos, uint32_t data)
{
    if(pos+3 < c->memory_kb * 1024) {
        c->ram[pos]   = (uint8_t)data;
        c->ram[pos+1] = (uint8_t)(data >> 8);
        c->ram[pos+2] = (uint8_t)(data >> 16);
        c->ram[pos+3] = (uint8_t)(data >> 24);
    } else {
        syslog(LOG_WARNING, "Trying to set 32-bit memory position above memory size: 0x%08X", pos);
    }
}

/* }}} */

void
lvm_computer_destroy(struct LVM_Computer* c)
{
    free(c->ram);
    free(c);
}


size_t lvm_computer_memorykb(struct LVM_Computer* c)
{
    return c->memory_kb;
}
