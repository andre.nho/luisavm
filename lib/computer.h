#ifndef LUISAVM_COMPUTER_H_
#define LUISAVM_COMPUTER_H_

#include <stdint.h>
#include <stdlib.h>

struct LVM_Computer;

struct LVM_Computer* lvm_computer_create(size_t memory_kb);
void                 lvm_computer_destroy(struct LVM_Computer* c);

void     lvm_computer_reset(struct LVM_Computer* c);

size_t   lvm_computer_memorykb(struct LVM_Computer* c);

uint8_t  lvm_computer_get(struct LVM_Computer* c, uint32_t pos);
void     lvm_computer_set(struct LVM_Computer* c, uint32_t pos, uint8_t data);
uint16_t lvm_computer_get16(struct LVM_Computer* c, uint32_t pos);
void     lvm_computer_set16(struct LVM_Computer* c, uint32_t pos, uint16_t data);
uint32_t lvm_computer_get32(struct LVM_Computer* c, uint32_t pos);
void     lvm_computer_set32(struct LVM_Computer* c, uint32_t pos, uint32_t data);

#endif
