#include "args.h"

#include <syslog.h>

extern void hello(void);

int main(int argc, char* argv[])
{
    Config cfg;

    openlog("luisavm", LOG_PERROR|LOG_PID, LOG_USER);
    cfg = config_parse(argc, argv);
    (void) cfg;
    return 0;
}
