#include "args.h"

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include "args.h"

static void
display_help(FILE* f)
{
    fprintf(f, "luisavm " VERSION "\n");
    fprintf(f, "\n");
    fprintf(f, "Available options:\n");
    fprintf(f, "    -u, --ui           User interfaces (options: ncurses, xcb, glfw)\n");
    fprintf(f, "    -h, --help         Print this help and exit\n");
    fprintf(f, "    -v, --version      Print version and exit\n");
    exit(f == stdout ? EXIT_SUCCESS : EXIT_FAILURE);
}


static void
display_version(void)
{
    printf("luisavm " VERSION "\n");
    exit(EXIT_SUCCESS);
}


static UserInterface
select_uilib(const char* opt)
{
#define ERR { syslog(LOG_ERR, "%s library was not part of this compilation. Please install the library and recompile luisavm.\n", opt); exit(EXIT_FAILURE); }
    if (strcmp(opt, "glfw")) {
#ifdef HAVE_GLFW
        return GLFW;
#else
        ERR
#endif
    } else if (strcmp(opt, "xcb")) {
#ifdef HAVE_XCB
        return XCB;
#else
        ERR
#endif
    } else if (strcmp(opt, "ncurses")) {
#ifdef HAVE_NCURSES
        return NCURSES;
#else
        ERR
#endif
    } else {
        syslog(LOG_ERR, "library '%s' is not an supported option\n", opt);
        display_help(stderr);
        return 0;
    }
#undef ERR
}



Config 
config_parse(int argc, char* argv[])
{
    Config config = {
#ifdef HAVE_GLFW
        GLFW,
#elif defined(HAVE_XCB)
        XCB,
#else
        NCURSES,
#endif
    };

    static struct option long_options[] = {
        { "ui",      required_argument, 0, 'u' },
        { "help",    no_argument,       0, 'h' },
        { "version", no_argument,       0, 'v' },
        { NULL,      0,                 0, 0   },
    };


    while (1) {
        int idx = 0;
        int c = getopt_long(argc, argv, "u:hv", long_options, &idx);
        
        if (c == -1) {
            break;
        }

        switch (c) {
            case 'u':
                config.ui = select_uilib(optarg);
                break;

            case 'h':
                display_help(stdout);
                break;

            case 'v':
                display_version();
                break;

            case '?':
                display_help(stderr);
                break;

            default:
                syslog(LOG_ERR, "Invalid option '%c'.\n", c);
                abort();
        }

    }

    return config;
}
