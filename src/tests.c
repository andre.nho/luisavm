#include <stdbool.h>
#include <stdio.h>

#include "luisavm.h"

#define MEM_SIZE 16

/* {{{ test infrastructure */

size_t test_count = 1;
bool   error_match = false;

void _test(uint32_t test, uint32_t expected, const char* description)
{
    if(test != expected) {
        printf("not ok %lu - %s (expected 0x%X, found 0x%X)\n", 
                test_count, description, expected, test);
        error_match = true;
    } else {
        printf("ok %lu - %s\n", test_count, description);
    }
    ++test_count;
}

#define TEST(test, expected) _test(test, expected, #test);

/* }}} */

static void 
test_memory(struct LVM_Computer* c)
{
    printf("#\n");
    printf("# MEMORY\n");
    printf("#\n");

    TEST(lvm_computer_memorykb(c), MEM_SIZE);

    lvm_computer_set(c, 0x12, 0xAF);
    TEST(lvm_computer_get(c, 0x12), 0xAF);

    lvm_computer_set16(c, 0x34, 0xABCD);
    TEST(lvm_computer_get16(c, 0x34), 0xABCD);
    TEST(lvm_computer_get(c, 0x34), 0xCD);

    lvm_computer_set32(c, 0xAA, 0x12345678);
    TEST(lvm_computer_get32(c, 0xAA), 0x12345678);
    TEST(lvm_computer_get(c, 0xAA), 0x78);

    lvm_computer_reset(c);
    TEST(lvm_computer_get(c, 0x12), 0x0);
}


int main(void)
{
    struct LVM_Computer* c = lvm_computer_create(MEM_SIZE);
    test_memory(c);
    lvm_computer_destroy(c);
    return error_match ? 127 : 0;
}
