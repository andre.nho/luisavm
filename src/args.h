#ifndef CONFIG_H
#define CONFIG_H

typedef enum UserInterface { NCURSES, XCB, GLFW } UserInterface;

typedef struct Config {
    UserInterface ui;
} Config;

Config config_parse(int argc, char* argv[]);

#endif
