#
# package information
#
VERSION = 0.1

# 
# object files
# 
LIB_OBJ = lib/global.o lib/computer.o
SRC_OBJ = src/main.o src/args.o

#
# compilation options
#
CFLAGS += -Ilib -DVERSION=\"$(VERSION)\" -fpic -Wall

#
# compilation rules
#
all: luisavm

luisavm: $(SRC_OBJ) libluisavm.so
	$(CC) -o $@ $(SRC_OBJ) -L. -lluisavm $(LDFLAGS) -Wl,-rpath=.

libluisavm.so: $(LIB_OBJ)
	$(CC) -shared -o $@ $^ $(LDFLAGS)

luisavm_tests: src/tests.o libluisavm.so
	$(CC) -o $@ src/tests.o -L. -lluisavm $(LDFLAGS) -Wl,-rpath=.

test: CFLAGS += -O0 -g -ggdb3 -DDEBUG -fno-inline-functions @build/WARNINGS
test: luisavm_tests
	./luisavm_tests

# 
# other rules
#

debug: CFLAGS += -O0 -g -ggdb3 -DDEBUG -fno-inline-functions @build/WARNINGS
debug: all

checkleaks: luisavm_tests
	valgrind --leak-check=full ./luisavm_tests

clean:
	rm -f src/*.o lib/*.o libluisavm.so luisavm luisavm_tests
